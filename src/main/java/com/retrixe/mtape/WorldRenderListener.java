package com.retrixe.mtape;

import com.mojang.blaze3d.platform.GlStateManager;
import com.mojang.blaze3d.systems.RenderSystem;
import com.retrixe.mtape.points.Point3d;
import com.retrixe.mtape.points.Point3i;
import com.retrixe.mtape.render.RenderBlockFrame;
import com.retrixe.mtape.render.RenderColor;
import com.retrixe.mtape.render.RenderLine;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.render.BackgroundRenderer;
import net.minecraft.client.render.DiffuseLighting;
import net.minecraft.util.math.Vec3d;
import org.joml.Matrix4fStack;

public class WorldRenderListener {
    private final TapeController controller;
    private final RenderColor lookingAtColor = new RenderColor(0, 255, 0);
    private final RenderColor originColor = new RenderColor(255, 255, 0);
    private final RenderColor selectedColor = new RenderColor(255, 128, 0);
    private final RenderColor tapeLineColor = new RenderColor(255, 128, 0);

    public WorldRenderListener(TapeController controller) {
        this.controller = controller;
    }

    public void onRender() {
        // No need to run GL commands, M-Tape isn't running.
        if (!this.controller.isTapeEnabled() && this.controller.getSelected().isEmpty()) return;

        MinecraftClient mc = MinecraftClient.getInstance();
        if (mc.getCameraEntity() == null) return;
        Matrix4fStack matrixStack = RenderSystem.getModelViewStack();
        this.preRenderSetupGL(matrixStack, mc);

        Point3d lookingAt = this.controller.getLookingAt();
        Point3d originPos = this.controller.getOrigin();
        if (this.controller.isTapeEnabled()) {
            if (lookingAt != null) {
                RenderBlockFrame.setColor(this.lookingAtColor);
                RenderBlockFrame.setLineWidth(4.0D);
                RenderBlockFrame.render((float) lookingAt.x(), (float) lookingAt.y(), (float) lookingAt.z());
            }

            if (originPos != null) {
                RenderBlockFrame.setColor(this.originColor);
                RenderBlockFrame.setLineWidth(4.0D);
                RenderBlockFrame.render((float) originPos.x(), (float) originPos.y(), (float) originPos.z());
            }

            if (lookingAt != null && originPos != null) {
                RenderLine.setColor(this.tapeLineColor);
                RenderLine.setLineWidth(2.0D);
                RenderLine.render(
                        (float) originPos.x() + 0.5F, (float) originPos.y() + 0.5F, (float) originPos.z() + 0.5F,
                        (float) lookingAt.x() + 0.5F, (float) lookingAt.y() + 0.5F, (float) lookingAt.z() + 0.5F
                );
            }
        }

        RenderBlockFrame.setColor(this.selectedColor);
        RenderBlockFrame.setLineWidth(4.0D);

        for (Point3i point : this.controller.getSelected()) {
            RenderBlockFrame.render(point.x(), point.y(), point.z());
        }

        this.postRenderSetupGL(matrixStack);
    }

    private void preRenderSetupGL(Matrix4fStack matrixStack, MinecraftClient mc) {
        matrixStack.pushMatrix();
        DiffuseLighting.disableForLevel();
        RenderSystem.disableCull();
        RenderSystem.enableBlend();
        RenderSystem.blendFuncSeparate(
                GlStateManager.SrcFactor.SRC_ALPHA,
                GlStateManager.DstFactor.ONE_MINUS_SRC_ALPHA,
                GlStateManager.SrcFactor.ONE,
                GlStateManager.DstFactor.ZERO
        );
        // RenderSystem.disableTexture();
        RenderSystem.setShaderColor(1.0f, 1.0f, 1.0f, 1.0f);
        // RenderSystem.glMultiTexCoord2f(GL13.GL_TEXTURE1, 240.0F, 240.0F);
        RenderSystem.depthMask(false);
        BackgroundRenderer.toggleFog();
        RenderSystem.disableDepthTest();
        Vec3d cameraPos = mc.gameRenderer.getCamera().getPos();
        matrixStack.translate((float) -cameraPos.getX(), (float) -cameraPos.getY(), (float) -cameraPos.getZ());
        //RenderSystem.applyModelViewMatrix();
    }

    private void postRenderSetupGL(Matrix4fStack matrixStack) {
        BackgroundRenderer.toggleFog();
        RenderSystem.enableDepthTest();
        RenderSystem.depthMask(true);
        // RenderSystem.enableTexture();
        RenderSystem.disableBlend();
        RenderSystem.enableCull();
        DiffuseLighting.enableForLevel();
        matrixStack.popMatrix();
        //RenderSystem.applyModelViewMatrix();
    }
}
