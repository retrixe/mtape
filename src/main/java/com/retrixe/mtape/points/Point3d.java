package com.retrixe.mtape.points;

import net.minecraft.entity.Entity;

public class Point3d {
    private final double x;
    private final double y;
    private final double z;

    public Point3d(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public Point3d(Entity entity, float partialTicks) {
        this.x = entity.prevX + (entity.getX() - entity.prevX) * (double)partialTicks;
        this.y = entity.prevY + (entity.getY() - entity.prevZ) * (double)partialTicks;
        this.z = entity.prevZ + (entity.getZ() - entity.prevZ) * (double)partialTicks;
    }

    public double x() {
        return this.x;
    }

    public double y() {
        return this.y;
    }

    public double z() {
        return this.z;
    }
}
