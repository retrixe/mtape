package com.retrixe.mtape;

import com.retrixe.mtape.points.Point3d;
import com.retrixe.mtape.points.Point3i;

import java.text.DecimalFormat;
import java.util.HashSet;
import java.util.Set;

public class TapeController {
    private boolean tapeEnabled = false;
    private Point3d lookingAt;
    private Point3d origin;
    private final Set<Point3i> selected = new HashSet<>();

    public boolean isTapeEnabled() {
        return this.tapeEnabled;
    }

    public void setTapeEnabled(boolean tapeEnabled) {
        this.tapeEnabled = tapeEnabled;
    }

    public void toggleTapeEnabled() {
        this.tapeEnabled = !this.tapeEnabled;
    }

    public Point3d getLookingAt() {
        return this.lookingAt;
    }

    public void setLookingAt(Point3d lookingAt) {
        this.lookingAt = lookingAt;
    }

    public Point3d getOrigin() {
        return this.origin;
    }

    public void setOrigin(Point3d origin) {
        this.origin = origin;
    }

    public Set<Point3i> getSelected() {
        return this.selected;
    }

    public void toggleSelected(Point3d point) {
        if (point != null) {
            Point3i pointInt = new Point3i((int)point.x(), (int)point.y(), (int)point.z());
            if (this.selected.contains(pointInt)) {
                this.selected.remove(pointInt);
            } else {
                this.selected.add(pointInt);
            }
        }

    }

    public String measure() {
        if (this.lookingAt != null && this.origin != null) {
            double diffX = Math.abs(this.lookingAt.x() - this.origin.x());
            double diffY = Math.abs(this.lookingAt.y() - this.origin.y());
            double diffZ = Math.abs(this.lookingAt.z() - this.origin.z());
            double dist = Math.sqrt(diffX * diffX + diffY * diffY + diffZ * diffZ);
            return "" + (int)diffX + ", " + (int)diffY + ", " + (int)diffZ + " (Dist: " + (new DecimalFormat("#0.000")).format(dist) + ")";
        } else {
            return null;
        }
    }
}
