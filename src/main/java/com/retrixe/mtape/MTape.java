package com.retrixe.mtape;

import com.retrixe.mtape.points.Point3d;
import net.fabricmc.api.ClientModInitializer;
import net.fabricmc.fabric.api.client.event.lifecycle.v1.ClientTickEvents;
import net.fabricmc.fabric.api.client.keybinding.v1.KeyBindingHelper;
import net.fabricmc.fabric.api.client.rendering.v1.HudRenderCallback;
import net.fabricmc.fabric.api.client.rendering.v1.WorldRenderEvents;
import net.minecraft.block.AirBlock;
import net.minecraft.block.BlockState;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.option.KeyBinding;
import net.minecraft.client.util.InputUtil;
import net.minecraft.text.Text;
import net.minecraft.util.hit.BlockHitResult;
import net.minecraft.util.hit.HitResult;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;
import org.lwjgl.glfw.GLFW;

public class MTape implements ClientModInitializer {
	private KeyBinding keyBindOrigin = new KeyBinding( // Used to be R, but R is now unavailable.
			"key.mtape.origin", InputUtil.Type.KEYSYM, GLFW.GLFW_KEY_I, "category.mtape");
	private KeyBinding keyBindSelect = new KeyBinding( // Used to be F, but F is now unavailable.
			"key.mtape.select", InputUtil.Type.KEYSYM, GLFW.GLFW_KEY_G, "category.mtape");
	private KeyBinding keyBindPmMeasure = new KeyBinding( // Used to be L, but L is now unavailable.
			"key.mtape.pm_measure", InputUtil.Type.KEYSYM, GLFW.GLFW_KEY_B, "category.mtape");
	private TapeController controller;
	private WorldRenderListener worldRenderListener;

	private static MTape instance;

	public static MTape getInstance() {
		return instance;
	}

	@Override
	public void onInitializeClient() {
		// This code runs as soon as Minecraft is in a mod-load-ready state.
		// However, some things (like resources) may still be uninitialized.
		// Proceed with mild caution.
		instance = this;
		keyBindOrigin = KeyBindingHelper.registerKeyBinding(keyBindOrigin);
		keyBindSelect = KeyBindingHelper.registerKeyBinding(keyBindSelect);
		keyBindPmMeasure = KeyBindingHelper.registerKeyBinding(keyBindPmMeasure);
		this.controller = new TapeController();
		this.worldRenderListener = new WorldRenderListener(this.controller);
		WorldRenderEvents.AFTER_ENTITIES.register(context -> this.worldRenderListener.onRender());
		ClientTickEvents.END_CLIENT_TICK.register(client -> {
			boolean inGame = client.world != null;
			onTick(client, inGame);
		});
		HudRenderCallback.EVENT.register((drawContext, tickDelta) -> {
			String measureString = this.controller.measure();
			if (measureString != null) {
				MinecraftClient mc = MinecraftClient.getInstance();
				int strWidth = mc.textRenderer.getWidth(measureString);
				int x = mc.getWindow().getScaledWidth() / 2 - strWidth / 2;
				int y = mc.getWindow().getScaledHeight() / 2 - 14;
				drawContext.drawText(mc.textRenderer, measureString, x, y, 16755200, false);
			}
		});
	}

	public void onTick(MinecraftClient mc, /* float partialTicks, */ boolean inGame) {
		if (inGame && mc.currentScreen == null // Avoid NPEs:
				&& mc.crosshairTarget != null && mc.world != null && mc.player != null) {
			// Point3i pointI = this.getBlockLookingAt(mc.getCameraEntity(), partialTicks);
			Vec3d pointI = mc.crosshairTarget.getPos(); // TODO: Extend reach to 16 blocks?
			Point3d point = pointI == null || mc.crosshairTarget.getType() != HitResult.Type.BLOCK
					? null : new Point3d(((BlockHitResult) mc.crosshairTarget).getBlockPos().getX(),
					((BlockHitResult) mc.crosshairTarget).getBlockPos().getY(),
					((BlockHitResult) mc.crosshairTarget).getBlockPos().getZ()
			);
			if (point != null) {
				BlockPos pos = ((BlockHitResult) mc.crosshairTarget).getBlockPos();
				BlockState blockstate = mc.world.getBlockState(pos);
				if (blockstate.getBlock() instanceof AirBlock) {
					point = null;
				}
			}

			this.controller.setLookingAt(point);
			String measureString;
			if (this.keyBindOrigin.wasPressed()) {
				if (!mc.player.isSneaking()) { // RSHIFT and LSHIFT
					if (this.controller.isTapeEnabled()) {
						this.controller.setOrigin(point); // Visual cue.
					}
				} else {
					this.controller.toggleTapeEnabled();
					this.controller.setOrigin(null);
					String resp = this.controller.isTapeEnabled() ? "Enabled" : "Disabled";
					Text message = Text.literal("§8[§e!§8] §eMTape §8[§e!§8] §7" + resp + " M-Tape.");
					mc.player.sendMessage(message, false);
				}
			} else if (this.keyBindSelect.wasPressed()) {
				if (!mc.player.isSneaking()) { // RSHIFT and LSHIFT
					if (this.controller.isTapeEnabled()) {
						this.controller.toggleSelected(point); // Visual cue.
					}
				} else {
					this.controller.getSelected().clear();
					Text reply = Text.literal("§8[§e!§8] §eMTape §8[§e!§8] §7Selected blocks cleared.");
					mc.player.sendMessage(reply, false);
				}
			} else if (this.keyBindPmMeasure.wasPressed() && this.controller.isTapeEnabled() && point != null) {
				measureString = this.controller.measure();
				if (measureString != null) {
					Text message = Text.literal("§8[§e!§8] §eMTape §8[§e!§8] §7" + measureString);
					mc.player.sendMessage(message, false);
				}
			}
		}
	}

    /* Replaced with client.crosshairTarget.
	private Point3i getBlockLookingAt(Entity entity, float partialTicks) {
		HitResult mop = entity.raycast(16.0D, partialTicks, false);
		if (mop != null && mop.getPos() != null && mop.getType() == HitResult.Type.BLOCK) {
			BlockPos pos = new BlockPos(mop.getPos());
			return new Point3i(pos.getX(), pos.getY(), pos.getZ());
		} else {
			return null;
		}
	} */
}
