package com.retrixe.mtape.render;

import com.mojang.blaze3d.systems.RenderSystem;
import net.minecraft.client.gl.ShaderProgramKeys;
import net.minecraft.client.render.*;

public class RenderBlockFilledSmall {
    private static byte red;
    private static byte green;
    private static byte blue;
    private static byte alpha;

    public static void setColor(RenderColor color) {
        red = color.red();
        green = color.green();
        blue = color.blue();
        alpha = color.alpha();
    }

    public static void setColor(RenderColor color, byte alpha) {
        red = color.red();
        green = color.green();
        blue = color.blue();
        RenderBlockFilledSmall.alpha = alpha;
    }

    public static void render(float x, float y, float z) {
        float x0 = x + 0.35F;
        float y0 = y + 0.35F;
        float z0 = z + 0.35F;
        float x1 = x + 0.65F;
        float y1 = y + 0.65F;
        float z1 = z + 0.65F;
        Tessellator tess = Tessellator.getInstance();
        BufferBuilder renderer = tess.begin(VertexFormat.DrawMode.TRIANGLE_FAN, VertexFormats.LINES);
        RenderSystem.setShader(ShaderProgramKeys.RENDERTYPE_LINES);
        renderer.vertex(x0, y0, z0).color(red, green, blue, alpha).normal(0, 0, 0);
        renderer.vertex(x1, y0, z0).color(red, green, blue, alpha).normal(0, 0, 0);
        renderer.vertex(x1, y1, z0).color(red, green, blue, alpha).normal(0, 0, 0);
        renderer.vertex(x0, y1, z0).color(red, green, blue, alpha).normal(0, 0, 0);
        renderer.vertex(x0, y1, z1).color(red, green, blue, alpha).normal(0, 0, 0);
        renderer.vertex(x0, y0, z1).color(red, green, blue, alpha).normal(0, 0, 0);
        renderer.vertex(x1, y0, z1).color(red, green, blue, alpha).normal(0, 0, 0);
        renderer.vertex(x1, y0, z0).color(red, green, blue, alpha).normal(0, 0, 0);
        BufferRenderer.drawWithGlobalProgram(renderer.end());
        renderer = tess.begin(VertexFormat.DrawMode.TRIANGLE_FAN, VertexFormats.LINES);
        renderer.vertex(x1, y1, z1).color(red, green, blue, alpha).normal(0, 0, 0);
        renderer.vertex(x1, y0, z1).color(red, green, blue, alpha).normal(0, 0, 0);
        renderer.vertex(x0, y0, z1).color(red, green, blue, alpha).normal(0, 0, 0);
        renderer.vertex(x0, y1, z1).color(red, green, blue, alpha).normal(0, 0, 0);
        renderer.vertex(x0, y1, z0).color(red, green, blue, alpha).normal(0, 0, 0);
        renderer.vertex(x1, y1, z0).color(red, green, blue, alpha).normal(0, 0, 0);
        renderer.vertex(x1, y0, z0).color(red, green, blue, alpha).normal(0, 0, 0);
        renderer.vertex(x1, y0, z1).color(red, green, blue, alpha).normal(0, 0, 0);
        BufferRenderer.drawWithGlobalProgram(renderer.end());
    }
}
