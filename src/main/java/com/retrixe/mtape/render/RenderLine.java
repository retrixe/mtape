package com.retrixe.mtape.render;

import com.mojang.blaze3d.systems.RenderSystem;
import net.minecraft.client.gl.ShaderProgramKeys;
import net.minecraft.client.render.*;

public class RenderLine {
    private static byte red;
    private static byte green;
    private static byte blue;
    private static byte alpha;
    private static float lineWidth;

    public static void setLineWidth(double lineWidth) {
        RenderLine.lineWidth = (float)lineWidth;
    }

    public static void setColor(RenderColor color) {
        red = color.red();
        green = color.green();
        blue = color.blue();
        alpha = color.alpha();
    }

    public static void setColor(RenderColor color, byte alpha) {
        red = color.red();
        green = color.green();
        blue = color.blue();
        RenderLine.alpha = alpha;
    }

    public static void render(float x0, float y0, float z0, float x1, float y1, float z1) {
        Tessellator tess = Tessellator.getInstance();
        BufferBuilder renderer = tess.begin(VertexFormat.DrawMode.LINES, VertexFormats.LINES);
        RenderSystem.setShader(ShaderProgramKeys.RENDERTYPE_LINES);
        RenderSystem.lineWidth(lineWidth);
        renderer.vertex(x0, y0, z0).color(red, green, blue, alpha).normal(0, 0, 0);
        renderer.vertex(x1, y1, z1).color(red, green, blue, alpha).normal(0, 0, 0);
        BufferRenderer.drawWithGlobalProgram(renderer.end());
        RenderSystem.lineWidth(1.0f);
    }
}
