# M-Tape

A Fabric mod to measure distances. Originally written by GeeItsZee, ported to Fabric 1.16.4 from LiteLoader 1.10.2.

This mod currently supports Quilt and Fabric 1.16.x through 1.21.x. There is no plan to backport to Fabric 1.14/1.15 unless anyone requires it.

Join the Discord at: https://discord.gg/MFSJa9TpPS

![Example of measuring distance between blocks with M-Tape](https://cdn.discordapp.com/attachments/406148854829023232/832187045065392148/unknown.png)

## Installation

This mod requires [Fabric Loader](https://fabricmc.net/use/) and [Fabric API](https://www.curseforge.com/minecraft/mc-mods/fabric-api).

- Get Fabric Loader and Fabric API from the provided links and install it for your version of Minecraft, if you don't already have it.
- Download the appropriate version of M-Tape from [Modrinth](https://modrinth.com/mod/mtape), [CurseForge](https://www.curseforge.com/minecraft/mc-mods/mtape) or the [GitLab release page.](https://gitlab.com/retrixe/mtape/-/releases)
- Go to your `.minecraft` folder [(instructions to find it here)](https://help.minecraft.net/hc/en-us/articles/360035131551-Where-are-Minecraft-files-stored-).
- Create a `mods` folder if it doesn't already exist, then copy the Fabric API and M-Tape JARs into it.
- Start the `fabric-loader-xxx` version of Minecraft that you installed in the first step. The mod should be loaded.

## Usage

- M-Tape is an easy-to-use measuring tape.
- The main keybind to use is the `Set Origin` key (default: I).
- To enable/disable M-Tape, press SHIFT to sneak, then click `Set Origin`. It will begin highlighting the block you are looking at.
- To measure distances, set the origin by looking at the block you want to start measuring from, and clicking `Set Origin`.
- Voilà! The mod will then measure the distance between the origin and the block you look at. The distance will be displayed above the crosshair, and a line will be drawn in-game between the two blocks. You can disable M-Tape to stop measuring.

### Additional Features

If you want the distance to appear in chat as a static message, click the `Send Measurement In Chat` (default: B) key. This message will not change, of course. This does not send the distance to other players though.

M-Tape also allows you to select blocks to keep track of them. When M-Tape is enabled, you can use the `Select Block` (default: G) key to select/unselect the block you are looking at. Selected blocks are highlighted with orange and remain selected even after M-Tape is disabled. You can clear all selected blocks by sneaking and clicking `Select Block`, similar to disabling M-Tape.

## Issues

If you find any issue with the mod, report it at the issue tracker [here](https://gitlab.com/retrixe/mtape/-/issues) if it isn't already reported.

## Development

M-Tape is a regular Fabric Gradle project. To build this mod, use the `build` task to build a JAR in `build/libs` e.g. by running `./gradlew build` in CLI.
